// Creates a client that listens for input until it  //<>// //<>//
// gets a linefeed character. Received strings are printed in the console. 

import processing.net.*; 
Client myClient; 
String inString;
String address="127.0.0.1"; // local computer
//String address="10.0.2.15"; // my virtual box.
int portNumber=5000;

void setup() { 
  size (300, 100);
  // Connect to the local machine at port 10002.
  // This example will not run if you haven't
  // previously started a server on this port.
  println("Attempting to connect to "+address+":" + portNumber);
  myClient =null;
  println("Setup ok");
} // setup 

void draw() { 
  if (myClient!=null && myClient.active()) {
    int numAvailable = myClient.available();
    if (numAvailable > 0) { 
      background(0); 
      inString = myClient.readStringUntil('\n'); 
      //drop final end-of-line
      if (inString != null && inString.length() > 0) {
        inString = inString.substring(0, inString.length() - 1);
      }
      println(inString);
    }
  } else {
    println("Connecting....");
    myClient= new Client (this, address, portNumber);
  if (!myClient.active()) {
    println("*** Error connecting to "+address+":" + portNumber);
    println("*** Is there a server listening on this port at this address?");
  } else {
    println("Connected to "+address+":" + portNumber);
  }
    
  }
} 

void keyPressed() {
  println("Exiting");
  System.exit(-1);
}
