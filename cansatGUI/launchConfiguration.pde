public static class Lc { //Lc stands for Launch Configuration
  // Communication
  static final String address="127.0.0.1"; // local computer
  //static final String address="10.0.2.15"; // my virtual box.
  //static final String address ="192.168.43.94"; //(IPV4)
  static final int portNumber =5000;

  // Can geometry
  static final float pyramidHeight = 100; 
  static final boolean drawAxisOnCan = true;
  static final int axisLength = 250;
  static final float canHeight=225;

  // Debugging
  static final boolean printIMU_Values= false;
  static final boolean rotateCan=true;    // For debugging: keep local referential fixed, just to validate Gravity and Magnetic field directions.
  static final boolean drawCan=true;
  static final boolean showCO2= true;
  static final boolean showGravity=false;
  static final boolean showMagField=false;
  static final boolean showRawMagField=false;
  static final float gravityDivider=0.05;

  // Position map
  static final float latitudeCollege=50.805408;
  static final float longitudeCollege=4.340244;
  static final float latitudePhysicsLab=50.80469;
  static final float longitudePhysicsLab=4.340547;

  static final float latitudeOperation=50.4838220; // TO MODIFY
  static final float longitudeOperation=6.1834560;
  static float verticalRange = 3;
  static float horizontalRange = 3;

  //Operating site
  static final Position.OperatingPosition sitePosition = Position.OperatingPosition.OPERATION_SITE;  //PHYSICS_LAB or COLLEGE or OPERATION_SITE depending on the situation.
  //                                                                                ^^^^^^^^^^^^^^

  //Custom image background
  static final boolean imageBackground= true; //Warning: laggy
}
