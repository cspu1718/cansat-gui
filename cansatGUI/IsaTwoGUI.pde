public static class IsaTwoGUI {

  public float roll, yaw, pitch, pressure, temperature, altitude, co2, xPos, yPos, zPos, latitude, longitude, TL_lat, TL_long, TR_lat, TR_long, BR_lat, BR_long, BL_lat, BL_long, altitude_GPS, altitude_GPScorr;
  public long timestamp;
  public float caX, caY, caZ, cgX, cgY, cgZ, cmX, cmY, cmZ;
  public float raX, raY, raZ, rgX, rgY, rgZ, rmX, rmY, rmZ;
  public float fix;
  protected float previousCO2;

  public void update(String CSV_Line) {
    IsaTwoGroundRecord rec = new IsaTwoGroundRecord(CSV_Line);

    timestamp=rec.timestampTrim;

    roll=rec.roll;
    yaw=rec.yaw;
    pitch=rec.pitch;

    fix=rec.fix;

    pressure=rec.pressure;
    temperature=rec.temperature;
    altitude=rec.altitude;
    altitude_GPS=rec.altitude_GPS;
    altitude_GPScorr=rec.altitude_GPScorr;
    if (rec.co2!= 0) {
      co2=rec.co2;
      previousCO2=co2;
    }
    xPos=rec.xPos;
    yPos=rec.yPos;
    zPos=rec.zPos;

    latitude= rec.latitude;
    longitude= rec.longitude;

    TL_lat=rec.TL_lat;
    TL_long=rec.TL_long;
    TR_lat=rec.TR_lat;
    TR_long=rec.TR_long;
    BR_lat=rec.BR_lat;
    BR_long=rec.BR_long;
    BL_lat=rec.BL_lat;
    BL_long=rec.BL_long;

    caX=rec.caX;
    caY=rec.caY;
    caZ=rec.caZ;
    cgX=rec.cgX;
    cgY=rec.cgY;
    cgZ=rec.cgZ;
    cmX=rec.cmX;
    cmY=rec.cmY;
    cmZ=rec.cmZ;

    raX=rec.raX;
    raY=rec.raY;
    raZ=rec.raZ;
    rgX=rec.rgX;
    rgY=rec.rgY;
    rgZ=rec.rgZ;
    rmX=rec.rmX;
    rmY=rec.rmY;
    rmZ=rec.rmZ;
  }
}
